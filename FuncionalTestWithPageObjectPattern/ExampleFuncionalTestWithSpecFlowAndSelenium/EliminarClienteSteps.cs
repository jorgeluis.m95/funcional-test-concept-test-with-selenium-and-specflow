﻿using System;
using OpenQA.Selenium;
using OpenQA.Selenium.Chrome;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using TechTalk.SpecFlow;
using System.Collections.Generic;
using System.Linq;
using ExampleFuncionalTestWithSpecFlowAndSelenium.PageObjects;
using Newtonsoft;
using Newtonsoft.Json;
using ExampleFuncionalTestWithSpecFlowAndSelenium.Modelos;
namespace ExampleFuncionalTestWithSpecFlowAndSelenium
{

    [Binding]
    public class EliminarClienteSteps
    {

        private ManageCustomerPage manageCustomerPage;
        private DeleteCustomerPage deleteCustomerPage;

        private IWebDriver driver;


        public EliminarClienteSteps()
        {
            driver = new ChromeDriver();
            manageCustomerPage = new ManageCustomerPage(driver);
        }

        [Given(@"yo tengo que ir a la página de lista de cliente")]
        public void GivenYoTengoQueIrALaPaginaDeListaDeCliente()
        {
            manageCustomerPage.GotoListOfCustomers();

        }

        [When(@"yo presiono el boton eliminar del ultimo cliente de la lista")]
        public void WhenYoPresionoElBotonEliminarDelUltimoClienteDeLaLista()
        {

            var listaLabels = driver.FindElements(By.TagName("Label"));
            var idLastElement = manageCustomerPage.GetLastIdOfTheList();
            deleteCustomerPage = manageCustomerPage.DoClickInDeleteBottonOfLastItem(idLastElement);
            deleteCustomerPage.DoClickInBottonDelete();
        }
        
        [Then(@"el resultado es un item menos a la lista\.")]
        public void ThenElResultadoEsUnItemMenosALaLista_()
        {
            var containerCustomerTable = manageCustomerPage.GetContainerCustomerTable();
            Assert.IsNotNull(containerCustomerTable);
            driver.Close();
        }
    }
}
