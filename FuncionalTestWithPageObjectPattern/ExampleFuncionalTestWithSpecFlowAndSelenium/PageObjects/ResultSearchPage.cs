﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using OpenQA.Selenium.Support.PageObjects;
using OpenQA.Selenium;

namespace ExampleFuncionalTestWithSpecFlowAndSelenium.PageObjects
{
    public class ResultSearchPage
    {
        private IWebDriver driver;

        public ResultSearchPage(IWebDriver _driver)
        {
            driver = _driver;
        }

        [FindsBy(How = How.ClassName, Using = "LC20lb")]
        IWebElement TitleFirstResult;

        public string GetTextFirstResultOfSearch()
        {
            var ResultTitle = TitleFirstResult.Text.ToString();
            return ResultTitle;
        }

        public int getNumberOfResultInFirtsPage()
        {
            var ResultNumber = driver.FindElements(By.ClassName("LC20lb")).Count();
            return ResultNumber;
        }
        

    }
}
