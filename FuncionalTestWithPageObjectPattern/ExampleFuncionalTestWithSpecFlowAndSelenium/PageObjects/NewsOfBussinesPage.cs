﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using OpenQA.Selenium.Support.PageObjects;
using OpenQA.Selenium;


namespace ExampleFuncionalTestWithSpecFlowAndSelenium.PageObjects
{
    public class NewsOfBussinesPage
    {
        private IWebDriver driver;

        public NewsOfBussinesPage(IWebDriver _driver)
        {
            driver = _driver;
            PageFactory.InitElements(driver, this);
        }
        [FindsBy(How = How.ClassName, Using = "noticias")]
        private IWebElement NewsContainer;

        public int GetNumberOfNews()
        {
            var NewsNumber = NewsContainer.FindElements(By.ClassName("norestricted")).Count();
            return NewsNumber;
        }
    }
}
