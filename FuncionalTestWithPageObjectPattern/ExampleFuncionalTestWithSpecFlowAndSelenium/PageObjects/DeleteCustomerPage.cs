﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using OpenQA.Selenium.Support.PageObjects;
using OpenQA.Selenium;


namespace ExampleFuncionalTestWithSpecFlowAndSelenium.PageObjects
{
    public class DeleteCustomerPage
    {
        private IWebDriver driver;
        public DeleteCustomerPage(IWebDriver _driver)
        {
            driver = _driver;
            PageFactory.InitElements(driver, this);

        }

        [FindsBy(How = How.Id, Using = "Delete")]
        private IWebElement BtnDelete;


        public ManageCustomerPage DoClickInBottonDelete()
        {
            BtnDelete.Click();
            return new ManageCustomerPage(driver);
        }

    }
}
