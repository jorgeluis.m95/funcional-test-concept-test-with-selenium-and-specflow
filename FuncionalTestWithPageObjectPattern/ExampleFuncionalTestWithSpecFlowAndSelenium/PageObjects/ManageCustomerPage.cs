﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using OpenQA.Selenium;
using OpenQA.Selenium.Support.PageObjects;
namespace ExampleFuncionalTestWithSpecFlowAndSelenium.PageObjects
{
    public class ManageCustomerPage
    {

        private IWebDriver driver;
        public ManageCustomerPage(IWebDriver _driver)
        {
            driver = _driver;
            PageFactory.InitElements(driver, this);
        }

        [FindsBy(How = How.TagName, Using = "table")]
        private IWebElement containerCustomerTable;

        [FindsBy(How = How.XPath, Using = "/html/body/div[2]/p/a")]
        private IWebElement LinkCrearCliente;



        public void GotoListOfCustomers()
        {
            driver.Url = Constantes.URL_PORTAL_BANCO_LISTA_CLIENTES;
        }

        public CreateCustomerPage GoToCreateNewCustomer()
        {
            LinkCrearCliente.Click();
            return new CreateCustomerPage(driver);
        }

        public string GetIdOfNewCustomerOfTheList(string idElement)
        {
            var idNuevoCLiente = driver.FindElement(By.Id(idElement)).Text;
            return idNuevoCLiente;
        }

        public string GetLastIdOfTheList()
        {
            var IdLastElement = driver.FindElements(By.TagName("Label")).Last();
            return IdLastElement.Text.ToString();
        }

        public EditCustomerPage DoClicInEditBoton(string IdItem)
        {
            driver.FindElement(By.XPath("//a[@href='/Cliente/Edit/" + IdItem.ToString() + "']")).Click();
            return new EditCustomerPage(driver); 
        }

        public DeleteCustomerPage DoClickInDeleteBottonOfLastItem(string idItem)
        {
            driver.FindElement(By.XPath("//a[@href='/Cliente/Delete/" + idItem.ToString() + "']")).Click();
            return new DeleteCustomerPage(driver);
        }

        public IWebElement GetContainerCustomerTable()
        {
            return containerCustomerTable;
        }
    }
}
