﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using OpenQA.Selenium;
using OpenQA.Selenium.Support.PageObjects;
using ExampleFuncionalTestWithSpecFlowAndSelenium.Modelos;
using ExampleFuncionalTestWithSpecFlowAndSelenium.PageObjects;

namespace ExampleFuncionalTestWithSpecFlowAndSelenium.PageObjects
{
    public class CreateCustomerPage
    {
        private IWebDriver driver;

        public CreateCustomerPage(IWebDriver _driver)
        {
            driver = _driver;
            PageFactory.InitElements(driver, this);

        }

        [FindsBy(How = How.Id, Using = "txtIdentificacion")]
        IWebElement txtIdentificacion;

        [FindsBy(How = How.Id, Using = "txtRazonSocial")]
        IWebElement txtRazonSocial;

        [FindsBy(How = How.Id, Using = "txtCiudad")]
        IWebElement txtCiudad;


        [FindsBy(How = How.Id, Using = "txtTipoCliente")]
        IWebElement txtTipoCliente;

        [FindsBy(How = How.Id, Using = "txtNivelRiesgo")]
        IWebElement txtNivelRiesgo;

        [FindsBy(How = How.Id, Using = "btnCreate")]
        IWebElement BtnCreate;


        public void InsertValuesToInputsOfTheForm( string inputId, Cliente cliente)
        {
            switch (inputId)
            {
                case Constantes.IdentificadortxtId:
                    txtIdentificacion.SendKeys(cliente.Identificacion);
                    break;
                case Constantes.IdentificadortxtRazonSocial:
                    txtRazonSocial.SendKeys(cliente.RazonSocial);
                    break;
                case Constantes.IdentificadortxtCiudad:
                    txtCiudad.SendKeys(cliente.Ciudad);
                    break;
                case Constantes.IdentificadortxtTipoCliente:
                    txtTipoCliente.SendKeys(cliente.TipoCliente);
                    break;
                case Constantes.IdentificadortxtNivelRiesgo:
                    txtNivelRiesgo.SendKeys(cliente.NivelRiesgo);
                    break;
                default:
                    break;
            }
        }


        public ManageCustomerPage DoClickInCreateBotton()
        {
            BtnCreate.Click();
            return new ManageCustomerPage(driver);
        }

    }
}
