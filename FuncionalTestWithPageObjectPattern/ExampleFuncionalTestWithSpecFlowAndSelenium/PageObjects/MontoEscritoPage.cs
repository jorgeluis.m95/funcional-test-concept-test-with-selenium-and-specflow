﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using OpenQA.Selenium;
using OpenQA.Selenium.Support.PageObjects;

namespace ExampleFuncionalTestWithSpecFlowAndSelenium.PageObjects
{
    public class MontoEscritoPage
    {
        private IWebDriver driver;

        public MontoEscritoPage(IWebDriver _driver)
        {
            driver = _driver;
            PageFactory.InitElements(driver, this);
        }


        [FindsBy(How = How.Id, Using = "txtMonto")]
        private IWebElement txtMontoInput;

        [FindsBy(How = How.Id, Using = "MontoEscrito")]
        private IWebElement LabelConvertido;

        [FindsBy(How = How.Id, Using = "Convertir")]
        private IWebElement BtnConvertir;


        public void IngresarMonto(string monto)
        {
            txtMontoInput.SendKeys(monto);
        }

        public void ConvertMonto()
        {
            BtnConvertir.Click();
            
        }

        public string GetValueMontoConvert()
        {
            var valueMontoText = LabelConvertido.Text;
            return valueMontoText;
        }

    }
}
