﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using OpenQA.Selenium;
using OpenQA.Selenium.Support.PageObjects;


namespace ExampleFuncionalTestWithSpecFlowAndSelenium.PageObjects
{
    public class BancoHomePage
    {

        private IWebDriver driver { get;  set; }

        [FindsBy(How = How.XPath, Using ="/html/body/div[1]/div/div[2]/ul/li[6]/a")]
        private IWebElement montoEscritoPage;

        [FindsBy(How = How.XPath, Using = "/html/body/div[1]/div/div[2]/ul/li[5]/a")]
        private IWebElement managerCustomer;

        public BancoHomePage(IWebDriver _driver)
        {
            driver = _driver;
            PageFactory.InitElements(driver, this);
        }

        public void  GotoHomePage()
        {
            driver.Url = Constantes.URL_PORTAL_BANCO_LISTA_CLIENTES;
            
        }

        public MontoEscritoPage GotoPageMontoEscrito()
        {

            montoEscritoPage.Click();
            return new MontoEscritoPage(driver);
        }

        public ManageCustomerPage GotoManageCustomerPage()
        {
            managerCustomer.Click();
            return new ManageCustomerPage(driver);

        }



    }
}
