﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using OpenQA.Selenium.Support.PageObjects;
using OpenQA.Selenium;

namespace ExampleFuncionalTestWithSpecFlowAndSelenium.PageObjects
{
    public class GooglePage
    {
        IWebDriver driver;

        public GooglePage(IWebDriver _driver)
        {
            driver = _driver;
            PageFactory.InitElements(driver, this);

        }


        [FindsBy(How = How.Name,Using ="q")]
        IWebElement TxtCajaBusqueda;

        [FindsBy(How = How.Name, Using = "btnK")]
        IWebElement BtnBuscar;



        public void GotoGooglePage()
        {
            driver.Url = Constantes.URL_PORTAL_GOOGLE;
        }

        public void InsertTextInInputOfSearch(string texto)
        {
            TxtCajaBusqueda.SendKeys(texto);
        }

        public ResultSearchPage DoClicInSearchBotton()
        {
            BtnBuscar.Click();
            return new ResultSearchPage(driver);
        }


    }
}
