﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using OpenQA.Selenium.Support.PageObjects;
using OpenQA.Selenium;


namespace ExampleFuncionalTestWithSpecFlowAndSelenium.PageObjects
{
    public class ElColombianoPage
    {

        private IWebDriver driver;

        public ElColombianoPage(IWebDriver _webdriver)
        {
            driver = _webdriver;
            PageFactory.InitElements(driver, this);
        }

        [FindsBy(How =How.XPath,Using = "//*[@id='menu_menuportlet_WAR_newsportlet_INSTANCE_c00f1f64354347bd9431b85880a2e815']/div/ul/li[4]/div/a")]
        private IWebElement linkNegocios;


        public void GoToTheColombianoPage()
        {
            driver.Url = Constantes.URL_PORTAL_EL_COLOMBIANO;
        }

        public NewsOfBussinesPage GoToBussinesNewsPage()
        {
            linkNegocios.Click();
            return new NewsOfBussinesPage(driver);
        }



    }
}
