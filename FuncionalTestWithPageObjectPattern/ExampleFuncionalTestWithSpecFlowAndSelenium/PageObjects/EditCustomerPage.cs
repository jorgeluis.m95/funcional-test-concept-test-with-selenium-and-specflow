﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using OpenQA.Selenium.Support.PageObjects;
using OpenQA.Selenium;


namespace ExampleFuncionalTestWithSpecFlowAndSelenium.PageObjects
{
    public class EditCustomerPage
    {
        private IWebDriver driver;

        public EditCustomerPage(IWebDriver _driver)
        {
            driver = _driver;
            PageFactory.InitElements(driver, this);
        }


        [FindsBy(How = How.Id, Using = Constantes.IdentificadortxtRazonSocial)]
        IWebElement txtRazonSocial;

        [FindsBy(How = How.Id, Using = Constantes.IdentificadortxtCiudad)]
        IWebElement txtCiudad;

        [FindsBy(How = How.Id, Using = "Save")]
        IWebElement BtnSave;


        public void ClearInputsOfRazonSocialAndCity()
        {
            txtCiudad.Clear();
            txtRazonSocial.Clear();
        }

        public void UpdateInputsOfRazonSocialAndCity(string razonSocial, string city)
        {
            txtRazonSocial.SendKeys(razonSocial);
            txtCiudad.SendKeys(city);
        }

        public ManageCustomerPage DoClickInSaveBotton()
        {
            BtnSave.Click();
            return new ManageCustomerPage(driver);
        }

        public string GetNewValueOfInputCity()
        {
            return txtCiudad.GetAttribute("value");
        }

        public string GetNewValueOfInputRazonSocial()
        {
            return txtRazonSocial.GetAttribute("value");
        }

    }
}
