﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using OpenQA.Selenium;
using OpenQA.Selenium.Chrome;
using System;
using System.Collections.Generic;
using TechTalk.SpecFlow;
using ExampleFuncionalTestWithSpecFlowAndSelenium.PageObjects;

namespace ExampleFuncionalTestWithSpecFlowAndSelenium
{
    [Binding]
    public class IngresarValorNumericoACampoMontoEscritoSteps
    {


        IWebDriver driver;
        private MontoEscritoPage montoEscritoPage;
        private BancoHomePage homePage;
        public IngresarValorNumericoACampoMontoEscritoSteps()
        {
            driver = new ChromeDriver();
             homePage = new BancoHomePage(driver);
        }

        [Given(@"Yo ingreso un número entero postivo con valor de (.*) en el campo de texto")]
        public void GivenYoIngresoUnNumeroEnteroPostivoConValorDeEnElCampoDeTexto(int p0)
        {
            homePage.GotoHomePage();
            montoEscritoPage = homePage.GotoPageMontoEscrito();
            montoEscritoPage.IngresarMonto("20");
            montoEscritoPage.ConvertMonto();

        }
        
        [Given(@"Yo ingreso un número entero no positivo con valor de (.*) en el campo de texto")]
        public void GivenYoIngresoUnNumeroEnteroNoPositivoConValorDeEnElCampoDeTexto(int p0)
        {
            homePage.GotoHomePage();
            montoEscritoPage = homePage.GotoPageMontoEscrito();
            montoEscritoPage.IngresarMonto("-30");
            montoEscritoPage.ConvertMonto();
        }
        
        [Given(@"Yo ingreso un numero decimal positivo con valor de (.*) en el campo de texto")]
        public void GivenYoIngresoUnNumeroDecimalPositivoConValorDeEnElCampoDeTexto(Decimal p0)
        {
            homePage.GotoHomePage();
            montoEscritoPage = homePage.GotoPageMontoEscrito();
            montoEscritoPage.IngresarMonto("0.5");
            montoEscritoPage.ConvertMonto();

        }
        
        [Given(@"Yo ingreso la cadena de caracteres ""(.*)"" en el campo de texto")]
        public void GivenYoIngresoLaCadenaDeCaracteresEnElCampoDeTexto(string p0)
        {
            homePage.GotoHomePage();
            montoEscritoPage = homePage.GotoPageMontoEscrito();
            montoEscritoPage.IngresarMonto("fert");
            montoEscritoPage.ConvertMonto();
        }
        
        [When(@"Yo presiono el boton de convertir")]
        public void WhenYoPresionoElBotonDeConvertir()
        {
            driver.FindElement(By.Id("Convertir")).Click();
        }
        
        [Then(@"El resultado obtenido es el valor de veinte\.")]
        public void ThenElResultadoObtenidoEsElValorDeVeinte_()
        {
            var reultadoConversion = montoEscritoPage.GetValueMontoConvert();
            Assert.AreEqual(reultadoConversion,"veinte");
            
        }
        
        [Then(@"El resultado obtenido es el valor menos treinta\.")]
        public void ThenElResultadoObtenidoEsElValorMenosTreinta_()
        {
            var reultadoConversion = montoEscritoPage.GetValueMontoConvert();
            Assert.AreEqual(reultadoConversion,"menos treinta");
            
        }
        
        [Then(@"El resultado obtenido es el valor de menos cero punto cinco\.")]
        public void ThenElResultadoObtenidoEsElValorDeMenosCeroPuntoCinco_()
        {
            var reultadoConversion = montoEscritoPage.GetValueMontoConvert();
            Assert.AreEqual(reultadoConversion, "Error:El monto no es numérico");
            
        }
        
        [Then(@"El resultado obtenido es el valor de monto no es numérico\.")]
        public void ThenElResultadoObtenidoEsElValorDeMontoNoEsNumerico_()
        {
            var reultadoConversion = montoEscritoPage.GetValueMontoConvert();
            Assert.AreEqual(reultadoConversion, "Error:El monto no es numérico");
            
        }
        
        [AfterScenario]
        public void CloseBrowser()
        {
            driver.Quit();
        }
    }
}
