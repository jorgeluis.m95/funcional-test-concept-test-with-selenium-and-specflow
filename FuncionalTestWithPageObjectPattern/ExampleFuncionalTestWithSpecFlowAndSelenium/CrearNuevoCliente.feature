﻿Feature: CrearNuevoCliente
	yo como un vendedor, 
	quiero ingresar al sistema de banco, dirigirme a la opción de cliente y registrar un nuevo cliente
	para incrementar mi inventario de clientes.
	

@mytag
Scenario: cuando creo un nuevo cliente obtengo nuevo cliente en la lista de clientes.
	Given yo ingreso al portal de administración de clientes 
	And yo ingreso al formulario de nuevo cliente
	And yo ingreso la identificacion del cliente
	And yo ingreso la razon social del cliente con valor "de Metales Max"
	And yo ingreso la ciudad del cliente con valor de "medellin"
	And yo ingreso el tipo de cliente con valor de "Juridico"
	And yo ingreso el riesgo del cliente valor de "Bajo"
	When yo creo el nuevo cliente
	Then valido si tengo nuevo cliente creado en la lista de clientes
	

