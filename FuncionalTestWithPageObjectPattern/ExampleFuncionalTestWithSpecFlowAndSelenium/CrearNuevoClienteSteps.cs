﻿using System;
using TechTalk.SpecFlow;
using ExampleFuncionalTestWithSpecFlowAndSelenium.Modelos;
using OpenQA.Selenium;
using OpenQA.Selenium.Chrome;
using ExampleFuncionalTestWithSpecFlowAndSelenium.PageObjects;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace ExampleFuncionalTestWithSpecFlowAndSelenium
{
    [Binding]
    public class CrearNuevoClienteSteps
    {
        private Cliente cliente;
        private CreateCustomerPage createcustomerPage;
        private ManageCustomerPage managerCustomerPage;
        private IWebDriver webDriver;

        public CrearNuevoClienteSteps()
        {
            cliente = new Cliente();
            webDriver = new ChromeDriver();
            managerCustomerPage = new ManageCustomerPage(webDriver);
            
        }

        [Given(@"yo ingreso al portal de administración de clientes")]
        public void GivenYoIngresoAlPortalDeAdministracionDeClientes()
        {
            managerCustomerPage.GotoListOfCustomers();   
        }
        
        [Given(@"yo ingreso al formulario de nuevo cliente")]
        public void GivenYoIngresoAlFormularioDeNuevoCliente()
        {
            createcustomerPage = managerCustomerPage.GoToCreateNewCustomer();
        }
        
        [Given(@"yo ingreso la identificacion del cliente")]
        public void GivenYoIngresoLaIdentificacionDelCliente()
        {
            cliente.Identificacion = Guid.NewGuid().ToString();
            createcustomerPage.InsertValuesToInputsOfTheForm("txtIdentificacion", cliente);
        }
        
        [Given(@"yo ingreso la razon social del cliente con valor ""(.*)""")]
        public void GivenYoIngresoLaRazonSocialDelClienteConValor(string p0)
        {
            cliente.RazonSocial = p0;
            createcustomerPage.InsertValuesToInputsOfTheForm("txtRazonSocial", cliente);
        }
        
        [Given(@"yo ingreso la ciudad del cliente con valor de ""(.*)""")]
        public void GivenYoIngresoLaCiudadDelClienteConValorDe(string p0)
        {
            cliente.Ciudad = p0;
            createcustomerPage.InsertValuesToInputsOfTheForm("txtCiudad", cliente);
        }
        
        [Given(@"yo ingreso el tipo de cliente con valor de ""(.*)""")]
        public void GivenYoIngresoElTipoDeClienteConValorDe(string p0)
        {
            cliente.TipoCliente = p0;
            createcustomerPage.InsertValuesToInputsOfTheForm("txtTipoCliente", cliente);
        }
        
        [Given(@"yo ingreso el riesgo del cliente valor de ""(.*)""")]
        public void GivenYoIngresoElRiesgoDelClienteValorDe(string p0)
        {
            cliente.NivelRiesgo = p0;
            createcustomerPage.InsertValuesToInputsOfTheForm("txtNivelRiesgo", cliente);
        }
        
        [When(@"yo creo el nuevo cliente")]
        public void WhenYoCreoElNuevoCliente()
        {
            managerCustomerPage = createcustomerPage.DoClickInCreateBotton();
        }
        
        [Then(@"valido si tengo nuevo cliente creado en la lista de clientes")]
        public void ThenValidoSiTengoNuevoClienteCreadoEnLaListaDeClientes()
        {
            var lastId = managerCustomerPage.GetLastIdOfTheList();
            Assert.AreEqual(lastId, cliente.Identificacion);
        }
    }
}
