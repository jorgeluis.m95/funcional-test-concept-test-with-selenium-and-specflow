﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ExampleFuncionalTestWithSpecFlowAndSelenium
{
    public class Constantes
    {
        public const string URL_PORTAL_BANCO_LISTA_CLIENTES = "https://ceibapruebasbanco.azurewebsites.net/Cliente";
        public const string URL_PORTAL_BANCO_MONTO_ESCRITO = "https://ceibapruebasbanco.azurewebsites.net/MontoEscrito";
        public const string URL_PORTAL_BANCO_EDIT_CLIENTE = "https://ceibapruebasbanco.azurewebsites.net/Cliente/Edit/";
        public const string URL_PORTAL_bANCO_DELETE_CLIENTE = "https://ceibapruebasbanco.azurewebsites.net/Cliente/Delete/";
        public const string URL_PORTAL_EL_COLOMBIANO = "http://www.elcolombiano.com";
        public const string URL_PORTAL_GOOGLE = "https://www.google.com";
        public const string URL_PORTAL_BANCO_CREAR_CLIENTE = "https://ceibapruebasbanco.azurewebsites.net/Cliente/Create";

        // identificadores de inputs en el formulario

        public const string IdentificadortxtId = "txtIdentificacion";
        public const string IdentificadortxtRazonSocial = "txtRazonSocial";
        public const string IdentificadortxtCiudad = "txtCiudad";
        public const string IdentificadortxtTipoCliente = "txtTipoCliente";
        public const string IdentificadortxtNivelRiesgo = "txtNivelRiesgo";




    }
}
