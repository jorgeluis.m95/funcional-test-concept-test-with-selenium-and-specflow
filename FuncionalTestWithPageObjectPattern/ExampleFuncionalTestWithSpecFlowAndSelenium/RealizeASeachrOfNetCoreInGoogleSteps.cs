﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using OpenQA.Selenium;
using OpenQA.Selenium.Chrome;
using System;
using System.Collections.Generic;
using TechTalk.SpecFlow;
using ExampleFuncionalTestWithSpecFlowAndSelenium.PageObjects;

namespace ExampleFuncionalTestWithSpecFlowAndSelenium
{
    [Binding]
    public class RealizeASeachrOfNetCoreInGoogleSteps
    {

        private IWebDriver webDriver { get; set; }
        private GooglePage googlePage;
        private ResultSearchPage ListaResultadoPage;
        public RealizeASeachrOfNetCoreInGoogleSteps()
        {
            webDriver = new ChromeDriver();
            var options = new ChromeOptions();
            options.AddArgument("--disable-single-click-autofill");
            googlePage = new GooglePage(webDriver);
        }




        [Given(@"I will to navigate a home page of google")]
        public void GivenIWillToNavigateAHomePageOfGoogle()
        {
            googlePage.GotoGooglePage();
           
        }
        
        [Given(@"I have entered \.Net core in the textbox of the home page")]
        public void GivenIHaveEntered_NetCoreInTheTextboxOfTheHomePage()
        {
            googlePage.InsertTextInInputOfSearch(".Net Core 2");
       
        }
        
        [When(@"I press search")]
        public void WhenIPressSearch()
        {
            ListaResultadoPage = googlePage.DoClicInSearchBotton();

        }
        
        [Then(@"the result should be a list of result about \.net core")]
        public void ThenTheResultShouldBeAListOfResultAbout_NetCore()
        {
            var texto = ListaResultadoPage.GetTextFirstResultOfSearch();
            var cantidad = ListaResultadoPage.getNumberOfResultInFirtsPage(); 

            Assert.AreEqual(texto, "NET Core - Microsoft");
            Assert.AreEqual(cantidad, 9);
            
            webDriver.Close();
        }


    }
}
