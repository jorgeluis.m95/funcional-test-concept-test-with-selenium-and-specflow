﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using OpenQA.Selenium;
using OpenQA.Selenium.Chrome;
using System;
using System.Collections.Generic;
using TechTalk.SpecFlow;
using ExampleFuncionalTestWithSpecFlowAndSelenium.PageObjects;

namespace ExampleFuncionalTestWithSpecFlowAndSelenium
{
    [Binding]
    public class IngresoToPageOfTheColombianoSteps
    {
        IWebDriver driver = new ChromeDriver();
        private ElColombianoPage PaginaElColombiano;
        private NewsOfBussinesPage noticasDeNegocio;
        public IngresoToPageOfTheColombianoSteps()
        {
            driver = new ChromeDriver();
            PaginaElColombiano = new ElColombianoPage(driver);
        }

        [Given(@"yo voy a la pagina principal del colombiano")]
        public void GivenYoVoyALaPaginaPrincipalDelColombiano()
        {
            PaginaElColombiano.GoToTheColombianoPage(); 
        }
        
        [When(@"y presiono la opcion de negocios")]
        public void WhenYPresionoLaOpcionDeNegocios()
        {
            noticasDeNegocio = PaginaElColombiano.GoToBussinesNewsPage();
        }
        
        [Then(@"obtengo como resultado una lista de noticias acerca de negocios")]
        public void ThenObtengoComoResultadoUnaListaDeNoticiasAcercaDeNegocios()
        {
            var NumeroArticulos = noticasDeNegocio.GetNumberOfNews();
            
            Assert.IsNotNull(NumeroArticulos);
            driver.Close();
        }
    }
}
