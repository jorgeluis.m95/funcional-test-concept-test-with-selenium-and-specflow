﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ExampleFuncionalTestWithSpecFlowAndSelenium.Modelos
{
    public class Cliente
    {
        public string Identificacion { get; set; }
        public string RazonSocial { get; set; }

        public string Ciudad { get; set; }
        public string TipoCliente { get; set; }
        public string NivelRiesgo { get; set; }
    }
}
