﻿using System;
using TechTalk.SpecFlow;
using OpenQA.Selenium;
using OpenQA.Selenium.Chrome;
using System.Collections.Generic;
using System.Linq;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using ExampleFuncionalTestWithSpecFlowAndSelenium.Modelos;
using ExampleFuncionalTestWithSpecFlowAndSelenium.PageObjects;

namespace ExampleFuncionalTestWithSpecFlowAndSelenium
{
    [Binding]
    public class ActualizarInformacionDeClienteSteps
    {
        IWebDriver driver;
        EditCustomerPage editCustomerPage;
        ManageCustomerPage manageCustomerPage;
        string valorRazonSocial = "";
        string valorCiudad = "";

        public ActualizarInformacionDeClienteSteps()
        {
            driver = new ChromeDriver();
            manageCustomerPage = new ManageCustomerPage(driver);

        }

        [Given(@"yo ingreso a la lista de clientes")]
        public void GivenYoIngresoALaListaDeClientes()
        {
            manageCustomerPage.GotoListOfCustomers();

        }
        
        [Given(@"yo presiono el boton de Edit")]
        public void GivenYoPresionoElBotonDeEdit()
        {
            var listaLabels =   driver.FindElements(By.TagName("Label"));
            var ultimoIdLista = manageCustomerPage.GetLastIdOfTheList();
            editCustomerPage = manageCustomerPage.DoClicInEditBoton(ultimoIdLista);

        }

        [Given(@"yo actualizo la razon social y la ciudad del cliente")]
        public void GivenYoActualizoLaRazonSocialYLaCiudadDelCliente()
        {
            editCustomerPage.ClearInputsOfRazonSocialAndCity();
            editCustomerPage.UpdateInputsOfRazonSocialAndCity("EFFFF", "Bogota");

            var txtCiudad = driver.FindElement(By.Name("Ciudad"));
            var txtRazonSocial = driver.FindElement(By.Name("RazonSocial"));
            valorCiudad = editCustomerPage.GetNewValueOfInputCity();
            valorRazonSocial = editCustomerPage.GetNewValueOfInputRazonSocial();
        }
        
        [When(@"y yo presiono en boton save")]
        public void WhenYYoPresionoEnBotonSave()
        {

            editCustomerPage.DoClickInSaveBotton();
        }
        
        [Then(@"obtengo los nuevos valores para el cliente actualizado")]
        public void ThenObtengoLosNuevosValoresParaElClienteActualizado()
        {
            var listaRegistros = driver.FindElements(By.TagName("Label"));
            var registroActualizado = listaRegistros.Last();

            Assert.AreEqual("EFFFF", valorRazonSocial);
            Assert.AreEqual("Bogota", valorCiudad);
            driver.Close();
        }
    }
}
