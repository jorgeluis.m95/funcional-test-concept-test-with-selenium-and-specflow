﻿Feature: EliminarCliente
	yo como vendedor
	quiero ingresar al portal de banco y en la lista de clientes, 
	para eliminar el ultimo cliente de la lista

@mytag
Scenario: Cuando ingreso a la lista de clientes obtengo eliminacion del ultimo cliente de la lista
	Given yo tengo que ir a la página de lista de cliente
	When yo presiono el boton eliminar del ultimo cliente de la lista
	Then el resultado es un item menos a la lista.
