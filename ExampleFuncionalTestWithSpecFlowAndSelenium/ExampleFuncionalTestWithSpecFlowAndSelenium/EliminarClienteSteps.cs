﻿using System;
using OpenQA.Selenium;
using OpenQA.Selenium.Chrome;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using TechTalk.SpecFlow;
using System.Collections.Generic;
using System.Linq;
namespace ExampleFuncionalTestWithSpecFlowAndSelenium
{

    [Binding]
    public class EliminarClienteSteps
    {
        IWebDriver driver = new ChromeDriver();

        [Given(@"yo tengo que ir a la página de lista de cliente")]
        public void GivenYoTengoQueIrALaPaginaDeListaDeCliente()
        {
            driver.Url = Constantes.URL_PORTAL_BANCO_LISTA_CLIENTES;

        }

        [When(@"yo presiono el boton eliminar del ultimo cliente de la lista")]
        public void WhenYoPresionoElBotonEliminarDelUltimoClienteDeLaLista()
        {
            var listaLabels = driver.FindElements(By.TagName("Label"));
            var lastElement = listaLabels.Last();

            var createClientOption = driver.FindElement(By.XPath("//a[@href='/Cliente/Delete/"+lastElement.Text.ToString()+"']"));
            
            createClientOption.Click();
            driver.FindElement(By.Id("Delete")).Click();
        }
        
        [Then(@"el resultado es un item menos a la lista\.")]
        public void ThenElResultadoEsUnItemMenosALaLista_()
        {
            
        }
    }
}
