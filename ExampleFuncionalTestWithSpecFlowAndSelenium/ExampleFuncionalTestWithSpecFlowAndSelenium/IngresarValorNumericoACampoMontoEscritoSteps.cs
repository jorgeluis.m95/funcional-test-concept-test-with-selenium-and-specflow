﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using OpenQA.Selenium;
using OpenQA.Selenium.Chrome;
using System;
using System.Collections.Generic;
using TechTalk.SpecFlow;

namespace ExampleFuncionalTestWithSpecFlowAndSelenium
{
    [Binding]
    public class IngresarValorNumericoACampoMontoEscritoSteps
    {

        IWebDriver driver = new ChromeDriver();
        [Given(@"Yo ingreso un número entero postivo con valor de (.*) en el campo de texto")]
        public void GivenYoIngresoUnNumeroEnteroPostivoConValorDeEnElCampoDeTexto(int p0)
        {
            driver.Url = Constantes.URL_PORTAL_BANCO_MONTO_ESCRITO;

            driver.FindElement(By.Id("txtMonto")).SendKeys("20");
        }
        
        [Given(@"Yo ingreso un número entero no positivo con valor de (.*) en el campo de texto")]
        public void GivenYoIngresoUnNumeroEnteroNoPositivoConValorDeEnElCampoDeTexto(int p0)
        {
            driver.Url = Constantes.URL_PORTAL_BANCO_MONTO_ESCRITO;
            driver.FindElement(By.Id("txtMonto")).SendKeys("-30");
        }
        
        [Given(@"Yo ingreso un numero decimal positivo con valor de (.*) en el campo de texto")]
        public void GivenYoIngresoUnNumeroDecimalPositivoConValorDeEnElCampoDeTexto(Decimal p0)
        {
            driver.Url = Constantes.URL_PORTAL_BANCO_MONTO_ESCRITO;
            driver.FindElement(By.Id("txtMonto")).SendKeys("0.5");
           
        }
        
        [Given(@"Yo ingreso la cadena de caracteres ""(.*)"" en el campo de texto")]
        public void GivenYoIngresoLaCadenaDeCaracteresEnElCampoDeTexto(string p0)
        {
            driver.Url = Constantes.URL_PORTAL_BANCO_MONTO_ESCRITO;
            driver.FindElement(By.Id("txtMonto")).SendKeys("fert");
        }
        
        [When(@"Yo presiono el boton de convertir")]
        public void WhenYoPresionoElBotonDeConvertir()
        {
            driver.FindElement(By.Id("Convertir")).Click();
        }
        
        [Then(@"El resultado obtenido es el valor de veinte\.")]
        public void ThenElResultadoObtenidoEsElValorDeVeinte_()
        {
            var reultadoConversion = driver.FindElement(By.Id("MontoEscrito")).Text;
            Assert.AreEqual(reultadoConversion,"veinte");
            driver.Close();
        }
        
        [Then(@"El resultado obtenido es el valor menos treinta\.")]
        public void ThenElResultadoObtenidoEsElValorMenosTreinta_()
        {
            var reultadoConversion = driver.FindElement(By.Id("MontoEscrito")).Text;
            Assert.AreEqual(reultadoConversion,"menos treinta");
            driver.Close();
        }
        
        [Then(@"El resultado obtenido es el valor de menos cero punto cinco\.")]
        public void ThenElResultadoObtenidoEsElValorDeMenosCeroPuntoCinco_()
        {
            var reultadoConversion = driver.FindElement(By.Id("MontoEscrito")).Text;
            Assert.AreEqual(reultadoConversion, "Error:El monto no es numérico");
            driver.Close();
        }
        
        [Then(@"El resultado obtenido es el valor de monto no es numérico\.")]
        public void ThenElResultadoObtenidoEsElValorDeMontoNoEsNumerico_()
        {
            var reultadoConversion = driver.FindElement(By.Id("MontoEscrito")).Text;
            Assert.AreEqual(reultadoConversion, "Error:El monto no es numérico");
            driver.Close();
        }
    }
}
