﻿Feature: IngresoToPageOfTheColombiano
	como un lector fiel del colombiano
	quiero ingresar a la página principal y dar clic en la sección de negocios
	para ver el listado de noticias

@mytag
Scenario: Cuando ingreso a la pagina principal del colombiano y doy clic en la seccion de negocios, obtengo lista de noticias sobre negocios
	Given yo voy a la pagina principal del colombiano
	When y presiono la opcion de negocios
	Then obtengo como resultado una lista de noticias acerca de negocios
