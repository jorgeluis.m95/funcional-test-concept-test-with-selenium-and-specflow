﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using OpenQA.Selenium;
using OpenQA.Selenium.Chrome;
using System;
using System.Collections.Generic;
using TechTalk.SpecFlow;

namespace ExampleFuncionalTestWithSpecFlowAndSelenium
{
    [Binding]
    public class RealizeASeachrOfNetCoreInGoogleSteps
    {
        IWebDriver webDriver;
        [Given(@"I will to navigate a home page of google")]
        public void GivenIWillToNavigateAHomePageOfGoogle()
        {

            webDriver = new ChromeDriver();
            webDriver.Url = Constantes.URL_PORTAL_GOOGLE;
           
        }
        
        [Given(@"I have entered \.Net core in the textbox of the home page")]
        public void GivenIHaveEntered_NetCoreInTheTextboxOfTheHomePage()
        {
            webDriver.FindElement(By.Name("q")).SendKeys(".Net Core 2");
        }
        
        [When(@"I press search")]
        public void WhenIPressSearch()
        {
            //webDriver.FindElement(By.Name("q")).Click();
            webDriver.FindElement(By.Name("btnK")).Click();
        }
        
        [Then(@"the result should be a list of result about \.net core")]
        public void ThenTheResultShouldBeAListOfResultAbout_NetCore()
        {
            var texto = webDriver.FindElement(By.ClassName("LC20lb")).Text.ToString();

            IList<IWebElement> results = webDriver.FindElements(By.ClassName("LC20lb"));

           var cantidad = results.Count;

            Assert.AreEqual(texto, "NET Core - Microsoft");
            Assert.AreEqual(cantidad, 9);
            
            webDriver.Close();
        }


    }
}
