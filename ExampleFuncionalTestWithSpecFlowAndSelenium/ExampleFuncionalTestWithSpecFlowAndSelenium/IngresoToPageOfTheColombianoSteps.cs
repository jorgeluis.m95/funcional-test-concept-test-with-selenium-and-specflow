﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using OpenQA.Selenium;
using OpenQA.Selenium.Chrome;
using System;
using System.Collections.Generic;
using TechTalk.SpecFlow;
namespace ExampleFuncionalTestWithSpecFlowAndSelenium
{
    [Binding]
    public class IngresoToPageOfTheColombianoSteps
    {
        IWebDriver driver = new ChromeDriver();

        [Given(@"yo voy a la pagina principal del colombiano")]
        public void GivenYoVoyALaPaginaPrincipalDelColombiano()
        {
            driver.Url = Constantes.URL_PORTAL_EL_COLOMBIANO;

        }
        
        [When(@"y presiono la opcion de negocios")]
        public void WhenYPresionoLaOpcionDeNegocios()
        {
            driver.FindElement(By.XPath("//*[@id='menu_menuportlet_WAR_newsportlet_INSTANCE_c00f1f64354347bd9431b85880a2e815']/div/ul/li[4]/div/a")).Click();
        }
        
        [Then(@"obtengo como resultado una lista de noticias acerca de negocios")]
        public void ThenObtengoComoResultadoUnaListaDeNoticiasAcercaDeNegocios()
        {
            var contenedorNoticias = driver.FindElement(By.ClassName("noticias"));
            var listaArticulosNoticias = driver.FindElements(By.ClassName("norestricted")).Count;

            Assert.IsNotNull(contenedorNoticias);
            Assert.IsNotNull(listaArticulosNoticias);
        }
    }
}
