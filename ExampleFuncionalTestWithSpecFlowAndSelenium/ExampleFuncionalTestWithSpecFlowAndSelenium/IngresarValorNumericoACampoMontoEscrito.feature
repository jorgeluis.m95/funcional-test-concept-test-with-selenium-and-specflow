﻿Feature: IngresarValorNumericoACampoMontoEscrito
	In order to avoid silly mistakes
	como un validador de montos
	yo quiero ingresar un valor numerico y obtener su valor en texto
@scenario1
Scenario: cuando ingreso un numero entero positivo obtengo su traduccion a texto
	Given Yo ingreso un número entero postivo con valor de 20 en el campo de texto
	When Yo presiono el boton de convertir
	Then El resultado obtenido es el valor de veinte.

@scenario2
Scenario: cuando ingreso un número entero no positivo obtengo su traduccion a texto
	Given Yo ingreso un número entero no positivo con valor de -30 en el campo de texto
	When Yo presiono el boton de convertir
	Then El resultado obtenido es el valor menos treinta.

@scenario3
Scenario: cuando ingreso un número con decimales obtengo texto El monto no es numérico
	Given Yo ingreso un numero decimal positivo con valor de 0.5 en el campo de texto
	When Yo presiono el boton de convertir
	Then El resultado obtenido es el valor de menos cero punto cinco.

@scenario4
Scenario: cuando ingreso una cadena de caracteres obtengo el texto El monto no es numérico
	Given Yo ingreso la cadena de caracteres "fert" en el campo de texto
	When Yo presiono el boton de convertir
	Then El resultado obtenido es el valor de monto no es numérico.




