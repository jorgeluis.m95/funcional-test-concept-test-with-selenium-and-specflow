﻿using System;
using TechTalk.SpecFlow;
using OpenQA.Selenium;
using OpenQA.Selenium.Chrome;
using System.Collections.Generic;
using System.Linq;
using Microsoft.VisualStudio.TestTools.UnitTesting;
namespace ExampleFuncionalTestWithSpecFlowAndSelenium
{
    [Binding]
    public class ActualizarInformacionDeClienteSteps
    {
        IWebDriver driver = new ChromeDriver();
        string valorRazonSocial = "";
        string valorCiudad = "";
        [Given(@"yo ingreso a la lista de clientes")]
        public void GivenYoIngresoALaListaDeClientes()
        {
            driver.Url = Constantes.URL_PORTAL_BANCO_LISTA_CLIENTES;

        }
        
        [Given(@"yo presiono el boton de Edit")]
        public void GivenYoPresionoElBotonDeEdit()
        {
            var listaLabels = driver.FindElements(By.TagName("Label"));
            var ultimoIdLista = listaLabels.Last();
            driver.FindElement(By.XPath("//a[@href='/Cliente/Edit/" +ultimoIdLista.Text.ToString() + "']")).Click();

        }

        [Given(@"yo actualizo la razon social y la ciudad del cliente")]
        public void GivenYoActualizoLaRazonSocialYLaCiudadDelCliente()
        {

            driver.FindElement(By.Name("RazonSocial")).Clear(); 
            driver.FindElement(By.Name("Ciudad")).Clear();
        
            driver.FindElement(By.Name("RazonSocial")).SendKeys("EFFFF");
            driver.FindElement(By.Name("Ciudad")).SendKeys("Bogota");

            var txtCiudad = driver.FindElement(By.Name("Ciudad"));
            var txtRazonSocial = driver.FindElement(By.Name("RazonSocial"));
            valorCiudad = txtCiudad.GetAttribute("value");
            valorRazonSocial = txtRazonSocial.GetAttribute("value");
        }
        
        [When(@"y yo presiono en boton save")]
        public void WhenYYoPresionoEnBotonSave()
        {
            
            driver.FindElement(By.Id("Save")).Click();
        }
        
        [Then(@"obtengo los nuevos valores para el cliente actualizado")]
        public void ThenObtengoLosNuevosValoresParaElClienteActualizado()
        {
            var listaRegistros = driver.FindElements(By.TagName("Label"));
            var registroActualizado = listaRegistros.Last();

            Assert.AreEqual("EFFFF", valorRazonSocial);
            Assert.AreEqual("Bogota", valorCiudad);
        }
    }
}
