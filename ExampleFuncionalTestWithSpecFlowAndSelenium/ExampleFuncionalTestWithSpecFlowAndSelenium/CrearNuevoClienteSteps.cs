﻿using System;
using TechTalk.SpecFlow;
using OpenQA.Selenium;
using OpenQA.Selenium.Chrome;
using System.Linq;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace ExampleFuncionalTestWithSpecFlowAndSelenium
{
    [Binding]
    public class CrearNuevoClienteSteps
    {

        IWebDriver driver = new ChromeDriver();
        string idTemporal = "";

        [Given(@"yo ingreso al portal de administración de clientes")]
        public void GivenYoIngresoAlPortalDeAdministracionDeClientes()
        {
            driver.Url = Constantes.URL_PORTAL_BANCO_LISTA_CLIENTES;
        }
        
        [Given(@"yo ingreso al formulario de nuevo cliente")]
        public void GivenYoIngresoAlFormularioDeNuevoCliente()
        {
            var crearNuevoClienteLink = driver.FindElement(By.XPath("/html/body/div[2]/p/a"));
            crearNuevoClienteLink.Click();
        }
        
        [Given(@"yo ingreso la identificacion del cliente")]
        public void GivenYoIngresoLaIdentificacionDelCliente()
        {
            idTemporal = Guid.NewGuid().ToString();
            driver.FindElement(By.Id("txtIdentificacion")).SendKeys(idTemporal);
        }
        
        [Given(@"yo ingreso la razon social del cliente")]
        public void GivenYoIngresoLaRazonSocialDelCliente()
        {
            driver.FindElement(By.Id("txtRazonSocial")).SendKeys(Guid.NewGuid().ToString());
        }
        
        [Given(@"yo ingreso la ciudad del cliente")]
        public void GivenYoIngresoLaCiudadDelCliente()
        {
            driver.FindElement(By.Id("txtCiudad")).SendKeys(Guid.NewGuid().ToString());
        }
        
        [Given(@"yo ingreso el tipo de cliente")]
        public void GivenYoIngresoElTipoDeCliente()
        {
            driver.FindElement(By.Id("txtTipoCliente")).SendKeys(Guid.NewGuid().ToString());
        }
        
        [Given(@"yo ingreso el riesgo del cliente")]
        public void GivenYoIngresoElRiesgoDelCliente()
        {
            driver.FindElement(By.Id("txtNivelRiesgo")).SendKeys(Guid.NewGuid().ToString());
        }
        
        [When(@"yo creo el nuevo cliente")]
        public void WhenYoCreoElNuevoCliente()
        {
            driver.FindElement(By.Id("btnCreate")).Click();
        }
        
        [Then(@"valido si tengo nuevo cliente creado en la lista de clientes")]
        public void ThenValidoSiTengoNuevoClienteCreadoEnLaListaDeClientes()
        {
            var listaLabls = driver.FindElements(By.TagName("label"));
            var ultimoLabel = listaLabls.Last();
            Assert.AreEqual(ultimoLabel.Text.ToString(), idTemporal);
            driver.Close();
        }
    }
}
